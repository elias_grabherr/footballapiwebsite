/*let urlTeams ="https://api.football-data.org/v2/competitions/2002/teams"

fetch(urlTeams, {
    method:"GET",

    headers:{
        "x-auth-token": "c400b9cd1a4b4a63b61294ccbd17dcbf"
    }
})
.then(response => response.json())
.then(function (data) {
    let html=""
    data.teams.forEach(element => {
        html += "<li><img src='" + element.crestURL + "'/>" + element.name + "</li>";

    });
    document.getElementById("teams").innerHTML = html;
});
*/

$(function () {
    torschuetzen();
    loadKader();
});

function torschuetzen() {

    $.ajax({
        url: "http://api.football-data.org/v2/competitions/2002/scorers/",
        headers: {
            "x-auth-token": "c400b9cd1a4b4a63b61294ccbd17dcbf"
        },
        method: "GET",
        success: function (data) {
            var html = "<ul id='scorer'></ul>";
            var html1 = "<ul id='goals'></ul>";
            
            data.scorers.forEach(element => {
                if (element.player!=null){
                    html += "<li>"+element.player.name +"</li>"
                    html1 += "<li>"+ element.numberOfGoals+ "</li>"
                }
                
            });
           html += "</ul>"
           html1 += "</ul>"
           $("#scorer").append(html);
           $("#goals").append(html1);
        },
        error: function (error) {
            alert("Die API ist nicht erreichbar, Fehler! Ratelimited!");
        }
    })
}

function loadKader() {
    $.ajax({
        url: "http://api.football-data.org/v2/teams/5",
        headers: {
            "x-auth-token": "c400b9cd1a4b4a63b61294ccbd17dcbf"
        },
        method: "GET",
        success: function (data) {
            var html = "<ul id='squad'></ul>";
            data.squad.forEach(element => {

                html += "<li>" + element.name + "</li>"
                console.log(element)


            });
            html += "</ul>"
            console.log(data);

            $("#squad").html(html);
        },
        error: function (error) {
            alert("Die API ist nicht erreichbar, Fehler! Ratelimited!");
        }
    })
}